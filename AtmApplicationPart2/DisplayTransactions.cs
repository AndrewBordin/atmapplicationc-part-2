﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmSharp
{
    /// <summary>
    /// Represents the displaying of the transactions. 
    /// Calls a mtheod which displays the list of transactions.
    /// </summary>
    class DisplayTransactions : Account
    {
         public void OnDisplayTransactions(Account account)
        {
          account.DisplayTransaction();
          return;
        }
    }
}
