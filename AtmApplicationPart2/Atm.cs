﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AtmSharp
{
    /// <summary>
    /// Valid user choices for the main menu
    /// </summary>
    enum MainMenuOption
    {
        SelectAccount = 1,
        CreateAccount,
        ExitAtmApplication
    }

    /// <summary>
    /// Valid user choices for the account menu
    /// </summary>
    enum AccountMenuOption
    {
        CheckBalance = 1,
        Withdraw,
        Deposit,
        DisplayTransactions,
        ExitAccount
    }

    /// <summary>
    /// The Atm class representing an ATM machine. The class displays and performs the the account management functions
    /// on a given bank account: checking balance, withdrawing and depositing money
    /// </summary>
    public class Atm
    {
        /// <summary>
        /// the bank this ATM object is working with
        /// </summary>
        private Bank _bank;

        /// <summary>
        /// The account object
        /// </summary>
        private Account acct;

        /// <summary>
        /// Holds the clients name
        /// </summary>
        private string clientName;

        /// <summary>
        /// Holds the initial deposit amount of the account upon creation
        /// </summary>
        public double initDepositAmount;

        /// <summary>
        /// Holds the Annual interest rate 
        /// </summary>
        private float annIntrRate;

        /// <summary>
        /// initializes the account type
        /// </summary>
        private AccountType acctType;


        private Account newAccount;

        /// <summary>
        /// Holds the account number
        /// </summary>
        private int acctNo;

        /// <summary>
        /// Holds the enum value that is selected.
        /// </summary>
        private AccountMenuOption selAcctMenuOpt;

        /// <summary>
        /// the list of previously done transactions
        /// </summary>
        public List<string> transactionList = new List<string>();
        //  private string acctTypeInput;

        /// <summary>
        /// Atm constructor that initializes the bank the ATM works with
        /// </summary>
        /// <param name="bank">the bank that manages the account objects the ATM works with</param>
        public Atm(Bank bank)
        {
            _bank = bank;
        }

        /// <summary>
        /// Starts the ATM program by displaying the required user options. 
        /// User navigates the menus managing their accounts
        /// </summary>
        public void Start()
        {
            //keep displaying the menu until the user chooses to exit the application
            while (true)
            {
                //display the main menu and perform the main actions depending on the user's choice
                MainMenuOption selectedOption;
                selectedOption = ShowMainMenu();

                // handle the user selection by calling the appropriate event handler method
                if (selectedOption == MainMenuOption.SelectAccount)
                {
                    acct = OnSelectAccount();

                    if (acct != null)
                    {
                        OnManageAccount(acct);
                    }
                }
                else if (selectedOption == MainMenuOption.CreateAccount)
                {
                    OnCreateAccount();
                }
                else if (selectedOption == MainMenuOption.ExitAtmApplication)
                {
                    //the application is shutting down, save all account information
                    _bank.SaveAccountData();
                    return;
                }
                else
                {
                    //go again when the user choose 3 instead of 1 or 2
                    Console.WriteLine("Please Enter a Valid Menu Option");
                }


            }
        }

        /// <summary>
        /// Displays the main ATM menu and ensure the user picks an option. Handles invalid input but doesn't check
        /// that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns>the option selected by the user</returns>
        private MainMenuOption ShowMainMenu()
        {
            //Display menu and obtain selection from user. Return selected option.
            while (true)
            {
                try
                {
                    Console.WriteLine("Main Menu, 1. Add account, 2. Create Account, 3. Exit, Enter a choice: ");
                    int choice = Convert.ToInt32(Console.ReadLine());
                    return (MainMenuOption)choice;
                }
                catch (InvalidValueException e)
                {
                    Console.WriteLine("Please enter a valid menu option", e);
                }
                return MainMenuOption.ExitAtmApplication;
            }

        }

        /// <summary>
        /// Displays the ACCOUNT menu that allows the user to perform account operations. Handles invalid input but doesn't check
        /// that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns>the option selected by the user</returns>
        private AccountMenuOption ShowAccountMenu()
        {
            //Display menu and obtain selection from user. Return selected option.
            while (true)
            {
                try
                {
                    Console.WriteLine("Account Menu, 1. Check Balance, 2. Withdraw, 3. Deposit, 4. Display Transactions 5. Exit, Enter a choice: ");
                    int accountChoice = Convert.ToInt32(Console.ReadLine());
                    return (AccountMenuOption)accountChoice;
                }
                catch (InvalidValueException e)
                {
                    Console.WriteLine("Please enter a valid menu option.", e);
                }
            }
        }

        /// <summary>
        /// Create and open an account. The user is prompted for all account information including the type of account to open.
        /// Create the account object and add it to the bank
        /// </summary>
        private void OnCreateAccount()
        {
            //repeat trying to create an account until the user is successful or gives up
            while (true)
            {
                try
                {
                    //get the name of the account holder from the user
                    clientName = PromptForClientName();

                    //get the initial deposit from the user
                    initDepositAmount = PromptForDepositAmount();

                    //get the annual interest rate from the user
                    annIntrRate = PromptForAnnualIntrRate();

                    //get the account type from the user
                    acctType = PromptForAccountType();

                    //open the account
                    newAccount = _bank.OpenAccount(clientName, acctType);

                    //set the other account propertites
                    newAccount.Deposit(initDepositAmount);
                    newAccount.SetAnnualIntrRate(annIntrRate);

                    //now the the account has been successfully created and added to the bank the method is done
                    return;
                }
                catch (InvalidValueException e)
                {
                    Console.WriteLine("Error", e);
                }
                catch (OperationCanceledException e)
                {
                    Console.WriteLine("Error", e);
                    return; //The user has cancelled the creation of the account
                }
            }
        }

        /// <summary>
        /// Select an account by prompting the user for an account number and remembering which account was selected.
        /// Prompt the user for performing account information such deposit and withdrawals
        /// </summary>
        /// <returns>
        ///     - the account object being selected or 
        ///     - null if the user cancelled; NOTE that this definition requires the caller to always use an if statement to check
        /// </returns>
        private Account OnSelectAccount()
        {
            //Attempt the user interaction  until all user information is provided correctly or the user cancels
            while (true)
            {
                try
                {
                    
                    //Prompt the user for the account number to select
                    Console.WriteLine("Please enter your account ID or press [ENTER] to cancel: ");
                    string acctNoInput = (Console.ReadLine());

                    //check to see if the user gave up and is cancelling the operation   
                    if (acctNoInput.Length == 0)
                    {
                        return null;
                    }

                    acctNo = Convert.ToInt32(acctNoInput);
                    //obtain the account required by the user from the bank
                    acct = _bank.FindAccount(acctNo);
                    //the user entered an account number get the actual number
                    if (acct != null)
                    {
                        return acct;
                    }
                    else
                    {
                        Console.WriteLine("The account was not found. Please select another account");
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("Please enter a valid account number (eg.100)", e);

                }
                return null;
            }
        }

        /// <summary>
        /// Manage the account by allowing the user to execute operation on the given account
        /// </summary>
        /// <param name="acct">the account to be managed</param>
        private void OnManageAccount(Account account)
        {
            //Attempt the user interaction  until all user information is provided correctly or the user cancels
            while (true)
            {
                selAcctMenuOpt = ShowAccountMenu();

                if (selAcctMenuOpt == AccountMenuOption.CheckBalance)
                {
                    OnCheckBalance(account);
                }
                else if (selAcctMenuOpt == AccountMenuOption.Withdraw)
                {
                    OnWithdraw(account);
                }
                else if (selAcctMenuOpt == AccountMenuOption.Deposit)
                {
                    OnDeposit(account);
                }
                else if (selAcctMenuOpt == AccountMenuOption.DisplayTransactions)
                {
                    var instance = new DisplayTransactions();
                    instance.OnDisplayTransactions(account);
                    
                }
                else if (selAcctMenuOpt == AccountMenuOption.ExitAccount)
                {
                    return;
                }
                else
                {
                    Console.WriteLine("Please enter a valid menu option");
                }
            }
        }

        /// <summary>
        /// Prompts the user to enter the name of the client and allows the user to cancel by pressing ENTER
        /// </summary>
        /// <returns>the name the client creating the account</returns>
        private string PromptForClientName()
        {
            //Prompt for teh client name
            Console.WriteLine("Please enter the client name or press [ENTER] to cancel: ");
            clientName = (Console.ReadLine());

            //check whether the user input was valid

            if (clientName.Length == 0)
            {
               throw new InvalidValueException("The user has selected to cancel the current operation");
               
            }

            return clientName;
        }

        /// <summary>
        /// Prompts the user to enter an account balance and performs basic error checking
        /// </summary>
        /// <returns>the amount to be deposited</returns>
        private double PromptForDepositAmount()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                try
                {
                    //Prompt for the initial balance
                    Console.WriteLine("Please enter your initial account balance: ");
                    initDepositAmount = Convert.ToDouble(Console.ReadLine());

                    //check the user input
                    if (initDepositAmount >= 0)
                    {
                        //if we got to this point the amount is valid
                        return initDepositAmount;
                    }
                    else if(Convert.ToString(initDepositAmount) == "")
                    {
                        Console.WriteLine("Please Enter a valid value");
                        
                    }
                    else
                    {
                        Console.WriteLine("Cannot Create an account with a negative balance. Please enter a valid amount");
                    }
                }
                catch (InvalidValueException e)
                {
                    Console.WriteLine("Please enter an initial account balance", e);
                   
                }

            }
        }

        /// <summary>
        /// Prompts the user to enter the annual interest rate for an account
        /// </summary>
        /// <returns>the annual interest rate for the account being created</returns>
        private float PromptForAnnualIntrRate()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter the interest rate for this account: ");
                    annIntrRate = float.Parse(Console.ReadLine());

                    //perform basic sanity checking of the input. Note that the business rules for checking are implemented
                    //in the account classes not here so that they are together with the rest of the account business logic
                    if (annIntrRate >= 0)
                    {
                        return annIntrRate;
                    }
                    else
                    {
                        Console.WriteLine("Cannot create an account with a negative interest rate.");
                    }
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        /// <summary>
        /// Prompts the user to enter an account type
        /// </summary>
        /// <returns>the account type as a constant as an enum value</returns>
        private AccountType PromptForAccountType()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //Prompt the user for the account type
                Console.WriteLine("Please enter the account type [c/s: chequing / savings]: ");
                string check = Console.ReadLine();
                string acctTypeInput = check.ToUpper();

                if (acctTypeInput == "C" || acctTypeInput == "CHEQUING" || acctTypeInput == "CHECKING")
                {
                    return AccountType.Chequing;
                }
                else if (acctTypeInput == "S" || acctTypeInput == "SAVINGS" || acctTypeInput == "SAVING")
                {
                    return AccountType.Savings;
                }
                else
                {
                    Console.WriteLine("Answer not supported. Please enter one of the supported answers.");
                }
            }
        }

        /// <summary>
        /// Prints the balance in the given account
        /// </summary>
        /// <param name="account">the account for which the balance is printed </param>
        private void OnCheckBalance(Account account)
        {
            //Display the current balance of the account
            Console.WriteLine("The balance is \n" + (account.GetBalance()));
        }

        /// <summary>
        /// Prompts the user for an amount and performs the deposit. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be deposited</param>
        private void OnDeposit(Account account)
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter an amount to deposit or type [ENTER] to exit: ");
                    string check = Console.ReadLine();

                    if (check.Length > 0)
                    {
                        double inputAmount = Convert.ToDouble(check);

                        account.Deposit(inputAmount);
                        account.Transaction("Deposited: ", inputAmount);
                        return;

                    }
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.");
                }
                catch (InvalidTransactionException e)
                {
                    Console.WriteLine(e);
                }
                
            }
            
        }

        /// <summary>
        /// Prompts the user for an amount and performs the withdrawal. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be withdrawn</param>
        private void OnWithdraw(Account account)
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter an amount to deposit or type [ENTER] to exit: ");
                    string check = Console.ReadLine();

                    if (check.Length > 0)
                    {
                        double inputAmount = Convert.ToDouble(check);

                        account.Withdraw(inputAmount);
                        account.Transaction("Withdrew: ", inputAmount);
                        return;
                    }
                }
                //if it gets to here, the user has entered a
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.");
                }
                catch (InvalidTransactionException e)
                {
                    Console.WriteLine(e);
                }
            }
        }


        /// <summary>
        /// Event handler called when the user selects to exit the application. Ensures that all account data is saved.
        /// </summary>
        private void OnExit()
        {
            //the application is shutting down, save all account information
        }

    }
}
